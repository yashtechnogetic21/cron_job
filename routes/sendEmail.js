const express = require("express");
const router = express.Router();
const nodemailer = require("nodemailer");
const csvtojson = require("csvtojson");
const SendEmail = require("../model/sendemail.model");
const User = require("../model/registration.model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
var multer = require("multer");
var upload = multer({ dest: "uploads/" });

const { check, validationResult } = require("express-validator");
const jwt_decode = require("jwt-decode");
const { wrapedSendMail_user, wrapedSendMail_admin } = require("./syncEmail");
let mailTransporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.USER_EMAIL,
    pass: process.env.USER_PASSWORD,
  },
});

router.post("/cronmail", upload.single("csvfile"), async (req, res) => {
  if (req.file == undefined || req.file.mimetype !== "text/csv") {
    return res.status(400).send({ message: "Please upload a CSV file!" });
  }

  const csvjsondata = csvtojson()
    .fromFile(req.file.path)
    .then((json) => {
      return json;
    });
  const autorun = async () => {
    try {
      await csvjsondata.map(async (data) => {
        mailTransporter.sendMail(
          {
            from: `${data.fromemail}`,
            to: `${data.toemail}`,
            subject: `${data.subject}`,
            html: `${data.message}`,
          },
          (err, info) => {
            if (err) {
              const sendEmaildetails = new SendEmail({
                fromemail: `${data.fromemail}`,
                toemail: `${data.toemail}`,
                status: `Failed`,
              });
              sendEmaildetails.save();
              return process.exit(1);
            }
            const sendEmaildetails = new SendEmail({
              fromemail: `${data.fromemail}`,
              toemail: `${data.toemail}`,
              status: `Successful`,
            });
            sendEmaildetails.save();
          }
        );
      });
    } catch (error) {
      console.log("err", error);
    }
  };

  setInterval(autorun, 1000 * 10);
  return res.status(201).send({
    status: 201,
    message: "Mail sending process started successfully",
  });
});

router.post(
  "/register",
  [
    check("name", "Name is required").not().isEmpty(),
    check("email", "Please include a valid email").isEmail(),
    check(
      "password",
      "Please enter a password with 6 or more characters"
    ).isLength({ min: 6 }),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (req.body.email && req.body.email.includes("@technogetic.com")) {
      if (!errors.isEmpty()) {
        return res.status(400).json({ message: "Error occurred" });
      }
    } else {
      return res
        .status(400)
        .json({ message: "Please enter valid technogetic mail" });
    }

    let user_email = req.body.email;
    let user = await User.findOne({ email: user_email });

    try {
      if (user) {
        return res.status(400).json({ message: "User already exist " });
      } else {
        const salt = await bcrypt.genSalt(10);
        const bcrypt_password = await bcrypt.hash(req.body.password, salt);
        const userdata = new User({
          name: req.body.name,
          email: req.body.email,
          password: bcrypt_password,
          verifyStatus: "",
        });

        const payload = {
          email: userdata.email,
          name: userdata.name,
        };
        const token = jwt.sign(payload, "mysecrettoken", { expiresIn: 36000 });

        await wrapedSendMail_user(req.body.email);
        await wrapedSendMail_admin(token);

        await userdata.save();

        return res
          .status(200)
          .json({ message: "User Registered Successfully done" });
      }
    } catch (error) {
      return res.status(400).json({ message: "Something wrong happens" });
    }
  }
);

router.post(
  "/login",
  [
    check("email", "Please include a valid email").isEmail(),
    check(
      "password",
      "Please enter a password with 6 or more characters"
    ).exists(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    const { email, password } = req.body;
    try {
      let user = await User.findOne({ email });

      if (!user) {
        return res.status(400).json({ message: "Invalid credentials" });
      }
      if (user.verifyStatus === "Accepted") {
        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
          return res.status(400).json({ message: "Invalid credentials" });
        }
        const payload = { id: user.id };
        jwt.sign(
          payload,
          "mysecrettoken",
          { expiresIn: 36000 },
          (err, token) => {
            if (err) throw err;
            return res.json({ message: "Login Successful", token });
          }
        );
      } else {
        return res
          .status(400)
          .json({ message: "You do not have permission to access this site" });
      }
    } catch (error) {
      return res.status(400).json({ message: "Something went wrong" });
    }
  }
);

router.post("/verifyUser", async (req, res) => {
  const { token, isVerified } = req.body;
  var decoded = jwt_decode(token);
  try {
    let user = await User.findOne({ email: decoded.email });

    // Verifying the JWT token
    if (isVerified === "Accepted") {
      mailOptions = {
        from: "yash.gupta@technogetic.com",
        to: `${decoded.email}`,
        subject: "User Verified Successfully",
        html: " User has been succesfully verified. You can login in our site at https://email-marketing-cron.vercel.app/",
      };
      mailTransporter.sendMail(mailOptions, function (err, data) {
        if (err) {
          console.log("Something wrong ", err);
        } else {
          console.log("Email sent successfully");
        }
      });
      user.verifyStatus = await isVerified;
      user.save();
      return res.status(200).json({ message: "Email Verified Succcessfully" });
    } else {
      mailOptions = {
        from: "yash.gupta@technogetic.com",
        to: `${decoded.email}`,
        subject: "User Request Rejected",
        html: "Your ",
      };
      mailTransporter.sendMail(mailOptions, function (error, data) {
        if (error) {
          console.log("Error Occurs", error);
        } else {
          console.log("Email sent successfully");
        }
      });
      await user.remove();
      return res.status(200).json({ message: "Email Verification Rejected" });
    }
  } catch (error) {
    return res.status(400).json({ message: "Something went wrong" });
  }
});

router.post("/forgotpassword", async (req, res) => {
  const { email } = req.body;
  let user = await User.findOne({ email });
  var user_token = "";
  const payload = {
    user: {
      id: user.id,
    },
  };
  jwt.sign(
    payload,
    "mysecrettoken",
    { expiresIn: 36000 },
    async (err, token) => {
      if (err) throw err;
      user_token = token;
      if (user_token || user) {
        try {
          mailOptions = {
            from: "amit.chaman@technogetic.com",
            to: `${email}`,
            subject: "Please verify my Email account",
            html: `Dear user please setup your reset password using this url :- https://email-marketing-cron.vercel.app/reset?token=${token}`,
          };

          mailTransporter.sendMail(mailOptions, function (err, data) {
            if (err) {
              return res.status(400).json({ message: "Something went wrong" });
            } else {
              return res
                .status(200)
                .json({ message: "Email sent successfully" });
            }
          });
        } catch (error) {
          return res.status(400).json({ message: "Something went wrong" });
        }
      } else {
        return res.status(400).json({
          message: "Your email does not match with registered email id ",
        });
      }
    }
  );
});

router.put(
  "/resetpassword",
  [
    check("token", "Please include a valid token").exists(),
    check(
      "password",
      "Please enter a password with 6 or more characters"
    ).exists(),
  ],
  async (req, res) => {
    try {
      const { token, password } = req.body;
      var decoded = jwt_decode(token);
      let user = await User.findOne({ email: decoded.email });
      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);
      await user.save();
      return res.status(200).json({ message: "password changed successfully" });
    } catch (error) {
      return res.status(400).json({ message: "Something went wrong" });
    }
  }
);

module.exports = router;
