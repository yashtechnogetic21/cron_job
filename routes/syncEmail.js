const nodemailer = require("nodemailer");
const dotenv = require("dotenv");
dotenv.config();

let mailTransporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.USER_EMAIL,
    pass: process.env.USER_PASSWORD,
  },
});
async function wrapedSendMail_user(email) {
  try {
    return new Promise((resolve, reject) => {
      mailOptions_user = {
        from: "yash.gupta@technogetic.com",
        to: `${email}`,
        subject: "Verification Pending",
        html: `Hi! There, You have register to our site. 
          Please wait for 2 business days to get your account verified. 
          After your account verification, you can do login`,
      };
      mailTransporter.sendMail(mailOptions_user, function (error, data) {
        if (error) {
          reject(false);
        }
        else {
          resolve(true)
        }
      });
    }
    )
  }
  catch (error) {
    console.log(error);
  }

}

async function wrapedSendMail_admin(token) {
  try {
    return new Promise((resolve, reject) => {
      mailOptions = {
        from: "yash.gupta@technogetic.com",
        to: "yash.gupta@technogetic.com",
        subject: "Please verify my Email account",
        html: `Hi! Admin please verify email by visiting the url
        https://email-marketing-cron.vercel.app/verifyUser?token=${token}
      Thanks`,
      };
      mailTransporter.sendMail(mailOptions, function (err, data) {
        if (err) {
          reject(false);
        } else {
          resolve(true);
        }
      });

    })
  }
  catch (error) {
    console.log(error);
  }
}


module.exports = { wrapedSendMail_admin, wrapedSendMail_user }