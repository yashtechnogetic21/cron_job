const express = require("express");
const fileupload = require("express-fileupload");
const app = express();
const connectDB = require("./config/db");
const router = require("./routes/sendEmail")
const PORT = 5000;
const cors = require('cors');
const dotenv = require("dotenv"); 
dotenv.config();
app.use(express.urlencoded({extended:false}));
app.use(cors());
app.use(express.json())
connectDB();
app.use(router);
app.use(fileupload());
app.use(express.static("files"));
app.listen(PORT, () => {  
  console.log(`server is listenning at port ${PORT}`);
});
  