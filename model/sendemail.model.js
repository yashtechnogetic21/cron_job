const mongoose = require("mongoose");
const SentEmailDetails = new mongoose.Schema({
  fromemail: {
    type: String,
    required: true,
  },
  toemail: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
});

const SentEmailDetailsSchema = mongoose.model(
  "Sent_Email_Details",
  SentEmailDetails
);
module.exports = SentEmailDetailsSchema;
